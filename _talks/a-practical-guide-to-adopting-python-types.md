---
abstract: Types in Python make it easier to understand how code works and helps catch
  bugs before they affect users, but require a significant upfront investment.  This
  talk will give a guide for quickly adopting types in large codebases to get to where
  types have more value than the cost of adding them!
duration: 30
level: Intermediate
room: PennTop South
slot: 2018-10-06 15:10:00-04:00
speakers:
- Roy Williams
title: A Practical Guide to Adopting Python Types
type: talk
---

Types in Python make it easier to understand how code works and helps catch bugs before they affect users, but require a significant upfront investment.  At first when a team embarks on adding types to an existing codebase, they frankly don't have a ton of value, because there's nothing to typecheck against other than typeshed, and if it's a well-tested codebase that already has users it likely doesn't have many glaring issues.  Eventually the codebase will have a critical mass of types where adding types is a no-brainer - everything else is typed so by adding a few annotations in the type signature helps ensure your code is correct.  

This talk will go over the basics of types in Python, the current ecosystem, and then go into detail on how teams can quickly type large chunks of their codebase using pyannotate - a tool that can be used to observe types from running code and apply those types to a codebase.  We'll also go over how to integrate types into CI to ensure the codebase remains safely typed.